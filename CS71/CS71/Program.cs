﻿using System;

namespace CS71
{
    class Program
    {
        static void Main(string[] args)
        {
            var hour=10;
            int min;
            int seconds;
            GetTime(out hour, out min, out seconds);

            GetTime(out int h, out int m, out int s);
            GetTime(out var h1, out var m1, out var s1);

            Console.WriteLine($"{hour}:{min}:{seconds}");

            Console.WriteLine($"{h}:{m}:{s}");
            Console.ReadLine();
            Console.WriteLine("Hello World!");
        }

        public static void GetTime(out int hour, out int min, out int seconds)
        {
            hour = 10;
            min = 40;
            seconds = 35;
        }
    }
}