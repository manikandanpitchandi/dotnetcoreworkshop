﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var name ="Raja";
            var age=10;

            var employeeList = new List<Employee>();

            employeeList.Add(new Employee { Name = "Test", Age = 25 });

            var foundEmploye = employeeList.FirstOrDefault(x=>x.Name=="Test");


            Console.WriteLine($"Age is : {foundEmploye?.Age ?? 10}");


            Console.WriteLine("Your name is :{0} and age is {1}", name, age);

            Console.WriteLine($"Your name is : {name} and age is {age}");
            Console.ReadLine();
        }

        public class Employee
        {
            public Employee()
            {
                this.Name = "No Name";
                this.Age = 40;
            }

            public string Name { get; set; } = "No Name";
            public int Age { get; set; } = 40;

            public int GetAgeInMonths()
            {
                return this.Age * 12;
            }

            public int GetAgeInMonthsNew() => this.Age * 12;
        }
    }
}
