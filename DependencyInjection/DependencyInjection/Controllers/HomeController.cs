﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DependencyInjection.Services;

namespace DependencyInjection.Controllers
{
    public class HomeController : Controller
    {
        private ITransientScope transient;
        private IRequestScope requestScope;
        private ISingletonScope singleton;
        private IProductService productService;
        public HomeController(IProductService productService, ITransientScope transient, IRequestScope requestScope, ISingletonScope singleton)
        {
            this.productService = productService;
            this.transient = transient;
            this.requestScope = requestScope;
            this.singleton = singleton;
        }
        public IActionResult Index()
        {
            var poducts = this.productService.GetProducts();

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
