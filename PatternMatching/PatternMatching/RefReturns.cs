﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    class RefReturns
    {
        public void Run()
        {
            int[] numbers = { 2, 7, 1, 9, 12, 8, 15 };
            ref int position = ref Substitute(12, numbers);
            position = -12;
            Console.WriteLine(numbers[4]);
        }

        public ref int Substitute(int value, int[] numbers)
        {
            for (int i = 0; i <= numbers.Length; i++)
            {
                if (numbers[i] == value)
                {
                    return ref numbers[i];
                }
            }

            throw new KeyNotFoundException();
        }
      
    }
}
